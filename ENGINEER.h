/*
 *	ENGINEER.h is a basement headerfile
 *	it includes some self-statement functions
 *	also some gtk+ frame functions
 */

#ifndef _ENGINEER_H_
#define _ENGINEER_H_

#define _ENGINEER_INSIDE_

#include<glib.h>
#include<gtk/gtk.h>
#include"E_frame/PublicTools.h"
#include"E_frame/BackGround.h"
#include"E_frame/UserLogin.h"

#endif 
#undef _ENGINEER_INSIDE_
